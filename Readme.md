# Backup des services

[Article du wiki](https://wiki.picasoft.net/doku.php?id=technique:adminsys:backup:start)

## Installer les systemd units sur la VM

On ne peut pas faire de lien symbolique car systemd scanne son dossier avant que `/DATA/docker` soit monté.
On doit donc copier ces fichiers :

```bash
sudo cp /DATA/docker/backups/autorestic.service /etc/systemd/system/
sudo cp /DATA/docker/backups/autorestic.timer /etc/systemd/system/

# demander à systemd de rescan son dossier
sudo systemctl daemon-reload

# demander à systemd de démarrer le timer au boot
sudo systemctl enable autorestic.timer

# demander à systemd de le démarrer maintenant
sudo systemctl start autorestic.timer
```
