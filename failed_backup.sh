#!/bin/sh

# Import environment inside .env
export $(cat /DATA/docker/backups/.env | xargs)

# Send a message to the alert channel of Picasoft's Mattermost team
curl -i -X POST -H "Content-Type: application/json" -d "{\"text\": \"$(hostname): backup failed for location ${1}\"}" ${HOOK_URL}
