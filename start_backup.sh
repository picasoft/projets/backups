#!/usr/bin/env sh

if [ ! -f /tmp/last_autorestic_check_date ]
then
	touch /tmp/last_autorestic_check_date
fi

current_date=$(date +"%D")
last_autorestic_check_date=$(cat /tmp/last_autorestic_check_date)

#todo: remove tls-client-cert flag when PR #253 is merged
/usr/local/bin/autorestic -c /DATA/docker/backups/$(hostname)/.autorestic.yaml --ci exec -av -- --tls-client-cert /DATA/docker/backups/$(hostname)/certs/bundle.pem unlock

#Check only one time a day
if [ "$current_date" != "$last_autorestic_check_date" ]
then
	#todo: use exec -- check when PR #253 is merged (more verbose)
	autorestic -c /DATA/docker/backups/$(hostname)/.autorestic.yaml check
	if [ $? -ne 0 ]
	then
		/DATA/docker/backups/notify_check_failed.sh
		exit
	fi
	echo $current_date > /tmp/last_autorestic_check_date
fi

/usr/local/bin/autorestic -vvv -c /DATA/docker/backups/$(hostname)/.autorestic.yaml --ci cron
